import itertools, sys


class Slitherlink:
    def __init__(self):
        self.rules = []
        self.size = 0



    def generate_clauses(self, input):
        for line in input:
            self.rules.append(line)
        self.size = len(self.rules) + 1

        for clause in self.number_rule():
            yield clause
        for clause in self.cycle_rule():
            yield clause



    def number_rule(self):
        '''
        Rule ensuring there are no more than n edges next to a number
        '''
        for x in range(self.size - 1):
            for y in range(self.size - 1):
                if self.rules[x][y] != 'x':
                    number = int(self.rules[x][y])
                    affected_edges = [f'{x}.{y}-{x + 1}.{y}', f'{x}.{y}-{x}.{y + 1}', f'{x + 1}.{y}-{x + 1}.{y + 1}', f'{x}.{y + 1}-{x + 1}.{y + 1}']

                    if number != 0:
                        clauses = itertools.combinations(affected_edges, 5 - number)
                        for clause in clauses:
                            yield ' '.join(clause)

                        not_clauses = itertools.combinations(affected_edges, number + 1)
                        for clause in not_clauses:
                            yield '~' + ' ~'.join(clause)

                    else:
                        for edge in affected_edges:
                            yield '~' + edge



    def cycle_rule(self):
        '''
        Rule ensuring every corner node has degree of either 0 or 2 (otherwise the solution won't have to be a cycle)
        '''
        for x in range(self.size):
            for y in range(self.size):
                neighbors = []
                # handling corner cases
                if x == 0:
                    if y == 0:
                        for (x_diff, y_diff) in [(1, 0), (0, 1)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 2
                    elif y == self.size - 1:
                        for (x_diff, y_diff) in [(1, 0), (0, -1)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 2
                    else:
                        for (x_diff, y_diff) in [(1, 0), (0, 1), (0, -1)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 3
                elif y == 0:
                    if x == self.size - 1:
                        for (x_diff, y_diff) in [(0, 1), (-1, 0)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 2
                    else:
                        for (x_diff, y_diff) in [(1, 0), (0, 1), (-1, 0)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 3
                elif x == self.size - 1:
                    if y == self.size - 1:
                        for (x_diff, y_diff) in [(-1, 0), (0, -1)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 2
                    else:
                        for (x_diff, y_diff) in [(0, 1), (-1, 0), (0, -1)]:
                            neighbors.append((x + x_diff, y + y_diff))
                        num_of_neighbors = 3
                elif y == self.size - 1:
                    for (x_diff, y_diff) in [(1, 0), (-1, 0), (0, -1)]:
                        neighbors.append((x + x_diff, y + y_diff))
                    num_of_neighbors = 3
                else:
                    for (x_diff, y_diff) in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                        neighbors.append((x + x_diff, y + y_diff))
                    num_of_neighbors = 4

                if num_of_neighbors == 2:
                    # (N1 ∨ ¬N2) ∧ (¬N1 ∨ N2)
                    yield f'{min(x, neighbors[0][0])}.{min(y, neighbors[0][1])}-{max(x, neighbors[0][0])}.{max(y, neighbors[0][1])}' + \
                          f' ~{min(x, neighbors[1][0])}.{min(y, neighbors[1][1])}-{max(x, neighbors[1][0])}.{max(y, neighbors[1][1])}'
                    yield f'~{min(x, neighbors[0][0])}.{min(y, neighbors[0][1])}-{max(x, neighbors[0][0])}.{max(y, neighbors[0][1])}' + \
                          f' {min(x, neighbors[1][0])}.{min(y, neighbors[1][1])}-{max(x, neighbors[1][0])}.{max(y, neighbors[1][1])}'
                else:
                    clause_combinations = list(itertools.combinations(neighbors, num_of_neighbors - 1))
                    not_all_neighbors = []
                    for neighbor in neighbors:
                        not_all_neighbors.append(f'~{min(x, neighbor[0])}.{min(y, neighbor[1])}-{max(x, neighbor[0])}.{max(y, neighbor[1])}')
                        for combination in clause_combinations:
                            if num_of_neighbors == 3:
                                yield f'~{min(x, neighbor[0])}.{min(y, neighbor[1])}-{max(x, neighbor[0])}.{max(y, neighbor[1])}' + \
                                f' {min(x, combination[0][0])}.{min(y, combination[0][1])}-{max(x, combination[0][0])}.{max(y, combination[0][1])}' + \
                                f' {min(x, combination[1][0])}.{min(y, combination[1][1])}-{max(x, combination[1][0])}.{max(y, combination[1][1])}'
                            else:
                                yield f'~{min(x, neighbor[0])}.{min(y, neighbor[1])}-{max(x, neighbor[0])}.{max(y, neighbor[1])}' + \
                                      f' {min(x, combination[0][0])}.{min(y, combination[0][1])}-{max(x, combination[0][0])}.{max(y, combination[0][1])}' + \
                                      f' {min(x, combination[1][0])}.{min(y, combination[1][1])}-{max(x, combination[1][0])}.{max(y, combination[1][1])}' + \
                                      f' {min(x, combination[2][0])}.{min(y, combination[2][1])}-{max(x, combination[2][0])}.{max(y, combination[2][1])}'
                    if num_of_neighbors == 3:
                        yield ' '.join(not_all_neighbors)
                    else:
                        combinations_of_three = list(itertools.combinations(not_all_neighbors, 3))
                        for combination in combinations_of_three:
                            yield ' '.join(combination)



    def check_solution(self, solution, SAT):
        '''
        Checks that the solution consists of only one cycle
        (would be too hard to do with CNF clauses)
        '''
        list_of_true_variables = []
        for variable_index in range(len(solution)):
            if solution[variable_index] == 1:
                list_of_true_variables.append((SAT.variables[variable_index].split('-')[0], SAT.variables[variable_index].split('-')[1]))
        current_edge = list_of_true_variables[0]
        starting_point = current_edge[0]
        position = current_edge[1]
        list_of_true_variables.remove(current_edge)

        while starting_point != position:

            for edge in list_of_true_variables:
                if position in edge[0]:
                    position = edge[1]
                    list_of_true_variables.remove(edge)
                    break
                elif position in edge[1]:
                    position = edge[0]
                    list_of_true_variables.remove(edge)
                    break
        if len(list_of_true_variables) == 0:
            return True
        else:
            return False



    def print_solution(self, solution, SAT, completed):
        grid = ''
        for x in range(self.size):
            grid += ' '
            next_row = ''
            for y in range(self.size):
                if x == self.size - 1:
                    if y == self.size - 1:
                        pass
                    else:
                        if solution[SAT.variable_values[f'{x}.{y}-{x}.{y + 1}']] and solution[SAT.variable_values[f'{x}.{y}-{x}.{y + 1}']] == 1:
                            grid += '⎯⎯⎯ '
                        else:
                            grid += '      '
                elif y == self.size - 1:
                    if solution[SAT.variable_values[f'{x}.{y}-{x + 1}.{y}']] and solution[SAT.variable_values[f'{x}.{y}-{x + 1}.{y}']] == 1:
                        next_row += f"⏐  {self.rules[x][y] if self.rules[x][y] != 'x' else ' '}"
                    else:
                        next_row += f"   {self.rules[x][y] if self.rules[x][y] != 'x' else ' '}"
                else:
                    if solution[SAT.variable_values[f'{x}.{y}-{x}.{y + 1}']] and solution[SAT.variable_values[f'{x}.{y}-{x}.{y + 1}']] == 1:
                        grid += '⎯⎯⎯ '
                    else:
                        grid += '      '
                    if solution[SAT.variable_values[f'{x}.{y}-{x + 1}.{y}']] and solution[SAT.variable_values[f'{x}.{y}-{x + 1}.{y}']] == 1:
                        next_row += f"⏐  {self.rules[x][y] if self.rules[x][y] != 'x' else ' '}  "
                    else:
                        next_row += f"   {self.rules[x][y] if self.rules[x][y] != 'x' else ' '}  "

            if x != self.size - 1:
                grid += '\n' + next_row

        print(grid)

        if not completed:
            for i in range(self.size * 2 - 1):
                sys.stdout.write("\033[F")
                sys.stdout.write("\033[K")
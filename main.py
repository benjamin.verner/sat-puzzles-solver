#!/usr/bin/python3
from sys import stderr
from plumbum import cli

from sat_class import SAT
from solver import Solver

import profile_function

class App(cli.Application):
    print_incomplete = cli.Flag(["progress"], help = "Show progress (slows down the process significantly) - does nothing with Logik puzzle")
    sudoku = cli.Flag(["sudoku"], help = "Normal Sudoku puzzle")
    abc = cli.Flag(["abc"], help="Easy as ABC puzzle")
    logik = cli.Flag(["logik"], help="Logik puzzle")
    slitherlink = cli.Flag(["slitherlink"], help="Slitherlink puzzle")



    def main(self, filename):
        with open(filename) as input:
            self.solve(input)



    # @profile_function.profile
    def solve(self, input):
        puzzle = SAT()
        if self.sudoku:
            puzzle.parse(input, 'sudoku')
        elif self.abc:
            puzzle.parse(input, 'abc')
        elif self.logik:
            puzzle.parse(input, 'logik')
        elif self.slitherlink:
            puzzle.parse(input, 'slitherlink')
        else:
            print('ERROR: No puzzle type selected.', file=stderr)
            return
        algorithm = Solver(puzzle)
        if self.print_incomplete:
            solutions = algorithm.find_solutions(puzzle, print_incomplete=True)
        else:
            solutions = algorithm.find_solutions(puzzle)
        solution_number = 0
        for solution in solutions:
            if self.slitherlink or self.logik:
                if puzzle.type.check_solution(solution, puzzle):
                    solution_number += 1
                    self.call_print_solution(solution, solution_number, puzzle)
            else:
                solution_number += 1
                self.call_print_solution(solution, solution_number, puzzle)
        if solution_number == 0:
            print('no solution found')



    def call_print_solution(self, solution, solution_number, puzzle):
        print(f'Solution #{solution_number}', file=stderr)
        print('##########')
        puzzle.print_solution(solution)
        print('##########\n')



if __name__ == '__main__':
    App.run()
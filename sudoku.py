import itertools, math, sys

from rich.console import Console
from rich.table import Table

class Sudoku:
    def __init__(self):
        self.rules = []
        self.variables = set()
        self.size = 0
        self.type = 0



    def generate_clauses(self, input):
        for line in input:
            self.rules.append(line)
        self.size = len(self.rules)
        for i in range(1, self.size + 1):
            self.variables.add(i)

        try:
            self.type = int(math.sqrt(self.size))
        except:
            raise Exception('Invalid puzzle given')

        for clause in self.filled_fields():
            yield clause
        for clause in self.position_rule():
            yield clause
        for clause in self.grid_rule():
            yield clause
        for clause in self.row_column_rule():
            yield clause



    def filled_fields(self):
        '''
        Rule ensuring fields filled in the assignment have given values
        '''
        if self.size == 16:
            letters_to_numbers = {'A': 10, 'B': 11,'C': 12, 'D': 13,'E': 14, 'F': 15, 'G': 16}
        x = 0
        for line in self.rules:
            y = 0
            for value in line:
                if value != '0' and value != '\n':
                    if ord('0') <= ord(value) <= ord('9'):
                        yield f'{value}-{x}.{y}'
                    else:
                        yield f'{letters_to_numbers[value]}-{x}.{y}'
                y += 1
            x += 1



    def position_rule(self):
        '''
        Rule ensuring there is at least one variable at a position
        '''
        for x in range(self.size):
            for y in range(self.size):
                clause = []
                for variable in self.variables:
                    clause.append(f'{variable}-{x}.{y}')
                yield ' '.join(clause)

        for x in range(self.size):
            for y in range(self.size):
                variable_couples = itertools.combinations(self.variables, 2)
                for couple in variable_couples:
                    yield f'~{couple[0]}-{x}.{y} ~{couple[1]}-{x}.{y}'




    def grid_rule(self):
        '''
        Rule ensuring there is no more than one same number in a grid
        (its size is dependant on the size of the problem)
        '''
        grid = set()
        for x in range(self.type):
            for y in range(self.type):
                grid.add((x,y))
        for x in range(self.type):
            for y in range(self.type):
                for variable in self.variables:
                    for position_1 in grid:
                        for position_2 in (grid - {position_1}):
                            yield f'~{variable}-{position_1[0] + self.type * x}.{position_1[1] + self.type * y} ~{variable}-{position_2[0] + self.type * x}.{position_2[1] + self.type * y}'



    def row_column_rule(self):
        '''
        Rule ensuring there is no more than one same number in a row and a column
        '''
        for coor in range(self.size):
            for variable in self.variables:
                for couple in list(itertools.combinations([i for i in range(self.size)], 2)):
                    yield f'~{variable}-{coor}.{couple[0]} ~{variable}-{coor}.{couple[1]}'
                    yield f'~{variable}-{couple[0]}.{coor} ~{variable}-{couple[1]}.{coor}'



    def print_solution(self, solution, SAT, completed):
        literals = []
        for i in range(len(solution)):
            if solution[i] == 1:
                literals.append(SAT.variables[i])
        grid = []
        for i in range(self.size):
            grid.append([None] * self.size)

        for literal in literals:
            info = literal.split('-')
            value = info[0]
            position = info[1].split('.')
            grid[int(position[0])][int(position[1])] = value
        
        border_table = Table(show_header=False, show_lines=True)
        border_table_grid = []

        positions = set()
        for x in range(self.type):
            for y in range(self.type):
                positions.add((x, y))

        for x in range(self.type):
            for y in range(self.type):
                # grid N x N (defines size of the inner tables)
                inner_table_grid = [[None for i in range(self.type)] for i in range(self.type)]
                for pos in positions:
                    if grid[pos[0] + self.type * x][pos[1] + self.type * y]:###
                        inner_table_grid[pos[0]][pos[1]] = grid[pos[0] + self.type * x][
                            pos[1] + self.type * y]
                    else:
                        inner_table_grid[pos[0]][pos[1]] = ' '
                border_table_grid.append(inner_table_grid)

        for i in range(self.type):
            border_table.add_column()
        a = 0
        row = []
        for table in border_table_grid:
            a += 1

            small_table = Table(show_header=False, show_edge=False, show_lines=True)
            for i in range(self.type):
                pomocny_list = []
                for q in range(self.type):
                    pomocny_list.append(table[i][q])
                small_table.add_row(*pomocny_list)
            row.append(small_table)

            if a % self.type == 0:
                border_table.add_row(*row)
                row = []

        console = Console()
        console.print(border_table)

        if not completed:
            for i in range(self.size * 2 + 1):
                sys.stdout.write("\033[F")
                sys.stdout.write("\033[K")
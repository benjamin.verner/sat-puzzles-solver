import itertools
import sys


class Logik:
    def __init__(self):
        self.letters = set()
        self.size = 0
        self.rules = []
        self.variables = set()
        self.special_vars = set()



    def generate_clauses(self, input):
        # splitting assignment
        line_number = 0
        for line in input:
            line_number += 1
            self.rules.append(line)
            word, dots = line.split()
            letter_position = {}
            position_index = 0
            line_letters = set()
            for letter in word:
                # in case there are multiple letters in the same line
                if letter in line_letters:
                    letter_position[letter].add(position_index)
                else:
                    letter_position[letter] = {position_index}
                self.letters.add(letter)
                line_letters.add(letter)
                position_index += 1
            self.size = len(word)

            for letter in self.letters:
                for index in range(self.size):
                    self.variables.add(f'{letter}-{index}')

            for clause in self.white_dot_rule(word, dots, letter_position):
                yield clause
            for clause in self.black_dot_rule(word, dots, letter_position):
                yield clause

        for clause in self.uniqueness_rule():
            yield clause
        for clause in self.must_be_rule():
            yield clause



    def white_dot_rule(self, word, dots, letter_position):
        '''
        Rule ensuring there is exactly the number of word letters at a different position as the number of white dots
        '''
        white_dots = dots.count('-')
        if white_dots > 0:
            variables = []
            variable_groups = [[] for _ in range(self.size)]
            for index in range(self.size):
                for letter_index in range(self.size):
                    if word[letter_index] != word[index]:
                        variables.append(f'{word[letter_index]}-{index}')
                        variable_groups[letter_index].append(f'{word[letter_index]}-{index}')
            white_clauses = list(itertools.combinations(variables, (self.size)*(self.size - 1) - white_dots + 1))
            for clause in white_clauses:
                yield ' '.join(clause)

            special_variables = []
            for i in range(self.size):
                x = 0
                while x in self.special_vars:
                    x += 1
                self.special_vars.add(x)
                special_variables.append(str(x))
            white_not_clauses = list(itertools.combinations(special_variables, self.size - white_dots + 1))
            for clause in white_not_clauses:
                yield  ' '.join(clause)

            for index in range(self.size):
                for variable in variable_groups[index]:
                    yield f'~{variable} ~{special_variables[index]}'
                yield f'{special_variables[index]} ' + ' '.join(variable_groups[index])

            # chci:
            # A -> ~1 ~2 ~3 = (~A ~1) (~A ~2) (~A ~3)
            # ~1 ~2 ~3 -> A = (1 2 3 A)



    def black_dot_rule(self, word, dots, letter_position):
        '''
        Rule ensuring there is exactly the number of word letters at a the given position as the number of black dots
        '''
        black_dots = dots.count('+')
        if black_dots > 0:
            variables = []
            for index in range(len(word)):
                variables.append(f'{word[index]}-{index}')
            black_clauses = list(itertools.combinations(variables, self.size + 1 - black_dots))
            for clause in black_clauses:
                yield  ' '.join(clause)
            black_not_clauses = list(itertools.combinations(variables, black_dots + 1))
            for clause in black_not_clauses:
                yield '~' + ' ~'.join(clause)
        else:
            for index in range(len(word)):
                yield f'~{word[index]}-{index}'



    def uniqueness_rule(self):
        '''
        Rule ensuring there is no more than one letter at a position
        '''
        for position in range(self.size):
            couples = list(itertools.combinations(self.letters, 2))
            for couple in couples:
                clause_string = f'~{couple[0]}-{position} ~{couple[1]}-{position}'
                yield clause_string



    def must_be_rule(self):
        '''
        Rule ensuring there is a letter at every position
        '''

        for index in range(self.size):
            clause = []
            for letter in self.letters:
                clause.append(f'{letter}-{index}')
            yield ' '.join(clause)



    def check_solution(self, solution, SAT):
        '''
        Checks rule that would be too hard to implement via clauses
        (that a letter isn't counted as both white and black)
        '''
        solution_variables = set()
        for variable in SAT.variables:
            if solution[SAT.variable_values[variable]] == 1:
                solution_variables.add(variable)
        solution = [None] * self.size
        for variable in solution_variables:
            if '-' in variable:
                letter, index = variable.split('-')
                solution[int(index)] = letter

        for line in self.rules:
            word, dots = line.split(' ')
            letters = []
            for letter in word:
                letters.append(letter)
            black_dot_count = dots.count('+')
            white_dot_count = dots.count('-')

            non_black_letters = []
            variables_left = []
            for i in range(self.size):
                if solution[i] == letters[i]:
                    black_dot_count -= 1
                else:
                    non_black_letters.append(letters[i])
                    variables_left.append(solution[i])

            if black_dot_count != 0:
                return False

            for letter in non_black_letters:
                if letter in variables_left:
                    white_dot_count -= 1
                    variables_left.remove(letter)

            if white_dot_count != 0:
                return False

        return True



    def print_solution(self, solution, SAT, completed):
        '''
        It is contraproductive to show progress of logik puzzle - the algorithm is too quick in this case
        '''
        if not completed:
            return

        for line in self.rules:
            word, dots = line.split()
            print(f'{word}', end=' | ')
            for dot in dots:
                if dot == '+':
                    print('◼', end='')
                elif dot == '-':
                    print('◻', end='')
            print('')

        for i in range(self.size):
            print('-', end='')
        print('')

        literals = []
        for i in range(len(solution)):
            if solution[i] == 1:
                literals.append(SAT.variables[i])

        final_word = [None] * self.size
        for literal in literals:
            if '-' in literal:
                letter, index = literal.split('-')
                final_word[int(index)] = letter

        for char in final_word:
            print(char, end='')
        print(' | ', end='')

        for i in range(self.size):
            print('◼', end='')

        print()
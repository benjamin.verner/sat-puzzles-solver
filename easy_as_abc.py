import itertools, sys

class ABC:
    def __init__(self):
        self.rules = []
        self.variables = {0}
        self.size = 0



    def generate_clauses(self, input):
        a = 0
        for line in input:
            a += 1
            if a == 1:
                for i in line:
                    if i != '\n':
                        self.variables.add(i)
            else:
                self.rules.append(line[:len(line) - 1])
        self.size = len(self.rules[0])

        yield self.rules

        for clause in self.starting_rule():
            yield clause
        for clause in self.position_rule():
            yield clause
        for clause in self.must_be_rule():
            yield clause
        for clause in self.row_column_rule():
            yield clause
        for clause in self.uniqueness_rule():
            yield clause



    def starting_rule(self):
        '''
        Starting letter rule
        '''

        rotate = 0
        for line in self.rules:
            for i in range(len(line)):
                if line[i] != '0':
                    if rotate == 0:
                        for list in self.generate_CNF_clauses(line[i], y = i):
                            clause = ' '.join(list)
                            yield clause
                    elif rotate == 1:
                        for list in self.generate_CNF_clauses(line[i], x = i, reverse = True):
                            clause = ' '.join(list)
                            yield clause
                    elif rotate == 2:
                        for list in self.generate_CNF_clauses(line[i], y = i, reverse = True):
                            clause = ' '.join(list)
                            yield clause
                    else:
                        for list in self.generate_CNF_clauses(line[i], x = i):
                            clause = ' '.join(list)
                            yield clause
            rotate += 1



    def generate_CNF_clauses(self, variable, x = False, y = False, reverse = False):
        '''
        x, y, reverse indicate where the rule literal is positioned and which direction the rule points
        (either x or y is to be given and is static)

        This rule takes advantage of the fact that a formula of the ABC rule in this form
        (for variables A,B,C, size of 6 and A as our literal in position 0 of the right column):

        (A-0.0) ∨ (A-0.1 ∧ 0-0.0) ∨ (A-0.2 ∧ 0-0.0 ∧ 0-0.1) ∨ (A-0.3 ∧ 0-0.0 ∧ 0-0.1 ∧ 0.0-2)

        can be rewritten into CNF form as:

        (A-0.0 ∨ 0-0.0) ∧ (A-0.0 ∨ A-0.1 ∨ 0-0.1) ∧ (A-0.0 ∨ A-0.1 ∨ A-0.2 ∨ 0-0.2) ∧ (A-0.0 ∨ A-0.1 ∨ A-0.2 ∨ A.0-3)

        '''

        clause_list = [[] for i in range(self.size - len(self.variables) + 2)]
        pointer = 0
        while pointer != (self.size - len(self.variables) + 2):
            for index in range(pointer, self.size - len(self.variables) + 2):
                if x is not False:
                    if not reverse:
                        clause_list[index].append(f'{variable}-{x}.{pointer}')
                    else:
                        clause_list[index].append(f'{variable}-{x}.{self.size - pointer - 1}')
                else:
                    if not reverse:
                        clause_list[index].append(f'{variable}-{pointer}.{y}')
                    else:
                        clause_list[index].append(f'{variable}-{self.size - pointer - 1}.{y}')
            pointer += 1
        for index in range(self.size - len(self.variables) + 1):
            if x is not False:
                if not reverse:
                    clause_list[index].append(f'0-{x}.{index}')
                else:
                    clause_list[index].append(f'0-{x}.{self.size - index - 1}')
            else:
                if not reverse:
                    clause_list[index].append(f'0-{index}.{y}')
                else:
                    clause_list[index].append(f'0-{self.size - index - 1}.{y}')

        for list in clause_list:
            yield list



    def must_be_rule(self):
        '''
        Rule ensuring that every variable is in every row and column
        Rule ensuring that every row and column has exactly (size - other variables) zeros
        '''

        for variable in self.variables - {0}:
            for x in range(self.size):
                clause1 = []
                for y in range(self.size):
                    clause1.append(f'{variable}-{x}.{y}')
                yield ' '.join(clause1)
            for y in range(self.size):
                clause1 = []
                for x in range(self.size):
                    clause1.append(f'{variable}-{x}.{y}')
                yield ' '.join(clause1)

        indexes = []
        for index in range(self.size):
            indexes.append(index)

        #min n clear boxes in a row or column

        # combinations without repetition
        # +2 bcs variables contain 0
        list_of_tuples = list(itertools.combinations(indexes, len(self.variables)))
        for x in range(self.size):
            for tuple_clause in list_of_tuples:
                clause1 = []
                clause2 = []
                for index in tuple_clause:
                    clause1.append(f'0-{x}.{index}')
                    clause2.append(f'0-{index}.{x}')
                yield ' '.join(clause1)
                yield ' '.join(clause2)

        #max n clear boxes in a row or column

        # combinations without repetition
        # +2 bcs variables contain 0
        list_of_tuples = list(itertools.combinations(indexes, self.size + 2 - len(self.variables)))
        for x in range(self.size):
            for tuple_clause in list_of_tuples:
                not_clause1 = []
                not_clause2 = []
                for index in tuple_clause:
                    not_clause1.append(f'~0-{x}.{index}')
                    not_clause2.append(f'~0-{index}.{x}')
                yield ' '.join(not_clause1)
                yield ' '.join(not_clause2)



    def row_column_rule(self):
        '''
        Rule ensuring that there is no more than 1 variable in each row and column
        '''

        mnozina = set()
        for i in range(self.size):
            mnozina.add(i)
        for coor in range(self.size):
            for variable in self.variables - {0}:
                for couple in list(itertools.combinations(mnozina, 2)):
                    yield f'~{variable}-{coor}.{couple[0]} ~{variable}-{coor}.{couple[1]}'
                    yield f'~{variable}-{couple[0]}.{coor} ~{variable}-{couple[1]}.{coor}'



    def position_rule(self):
        '''
        Rule ensuring that there is at least 1 variable at every position
        '''

        for x in range(self.size):
            for y in range(self.size):
                clause = []
                for variable in self.variables:
                    clause.append(f'{variable}-{x}.{y}')
                yield ' '.join(clause)



    def uniqueness_rule(self):
        '''
        Rule ensuring there is no more than 1 variable (including 0) at a position
        (technically not needed, but speeds up the process - the algorithm won't search variants
        in which 2 variables at a position are True)
        '''

        for x in range(self.size):
            for y in range(self.size):
                for couple in itertools.combinations(self.variables, 2):
                    clause = f'~{couple[0]}-{x}.{y} ~{couple[1]}-{x}.{y}'
                    yield clause



    def print_solution(self, solution, SAT, completed):
        literals = []
        for i in range(len(solution)):
            if solution[i] == 1:
                literals.append(SAT.variables[i])
        grid = []
        for i in range(self.size):
            grid.append([None] * self.size)

        for literal in literals:
            info = literal.split('-')
            value = info[0]
            position = info[1].split('.')
            grid[int(position[0])][int(position[1])] = value

        print('     ', end='')
        for i in self.rules[0]:
            if i != '0':
                print(f'   {i}  ', end='')
            else:
                print('      ', end='')
        print('\n      ', end='')
        for i in range(self.size):
            print('-----', end=' ')
        print('\n', end='')
        line_number = 0
        for line in range(self.size):
            if self.rules[3][line_number] != '0':
                print(f'  {self.rules[3][line_number]}  ', end='|')
            else:
                print('     ', end='|')
            for number in grid[line]:
                if number and number != '0':
                    print(f'  {number}  ', end='|')
                elif not number:
                    print(f'     ', end='|')
                else:
                    print('  x  ', end='|')
            if self.rules[1][line_number] != '0':
                print(f'  {self.rules[1][line_number]}  ', end='\n      ')
            else:
                print('\n      ', end='')
            for i in range(self.size):
                print('-----', end=' ')
            print('\n', end='')

            line_number += 1

        print('     ', end='')
        for i in self.rules[2]:
            if i != '0':
                print(f'   {i}  ', end='')
            else:
                print('      ', end='')
        print('')

        if not completed:
            for i in range(self.size * 2 + 3):
                sys.stdout.write("\033[F")
                sys.stdout.write("\033[K")

        a = '''
                C                 A           A   
              ----- ----- ----- ----- ----- -----
          B  |  x  |  B  |  C  |  A  |  x  |  x  |
              ----- ----- ----- ----- ----- -----
             |  C  |  x  |  A  |  x  |  B  |  x  |
              ----- ----- ----- ----- ----- -----
          A  |  x  |  A  |  B  |  C  |  x  |  x  |
              ----- ----- ----- ----- ----- -----
          B  |  B  |  C  |  x  |  x  |  x  |  A  |
              ----- ----- ----- ----- ----- -----
          B  |  x  |  x  |  x  |  B  |  A  |  C  |
              ----- ----- ----- ----- ----- -----
             |  A  |  x  |  x  |  x  |  C  |  B  |
              ----- ----- ----- ----- ----- -----
                A           B     B     C     B   
        '''
# SAT Puzzles Solver
Python implementation of SAT Solver algorithm with focus on Sudoku, Easy as ABC, Logik and Slitherlink puzzles.

## Installation
Clone repository with the following command:
```bash
$ git clone https://gitlab.com/benjamin.verner/sat-puzzles-solver
```

Install needed requirements:
```bash
$ pip install -r requirements.txt
```

Run the main.py file:
```bash
$ ./main.py --help
Usage:
    main.py [SWITCHES] filename

Meta-switches:
    -h, --help         Prints this help message and quits
    --help-all         Prints help messages of all sub-commands and quits
    -v, --version      Prints the program's version and quits

Switches:
    --abc              Easy as ABC puzzle
    --logik            Logik puzzle
    --progress         Show progress (slows down the process significantly)
    --slitherlink      Slitherlink puzzle
    --sudoku           Normal Sudoku puzzle
```

You need to have Python3 in order to successfully run the program.

## Citations

### Used libraries

[Itertools](https://docs.python.org/3/library/itertools.html)

Used in clause generating for creating combinations from a set of variables.

[Rich](https://pypi.org/project/rich/)

Used for output formatting is Sudoku puzzles to create nice Sudoku grids.

[Plumbum](https://pypi.org/project/plumbum/)

Used for running the app from console.

### Images in the documentation

- [Sudoku](https://towardsdatascience.com/how-to-win-sudoku-3a82d05a57d)

- [Easy as ABC](http://puzzleparasite.blogspot.com/2011/09/rules-easy-as-abc.html)

- [Slitherlink](https://hausigel.de/blog/2020/08/15/classics-collection-slitherlink/)

<!-- ### Running via virtual environment

Create Python virtual environment for running the App:
```bash
$ python3 -m venv ./venv
```

Activate it using the following command:
Unix:
```bash
$ . venv/bin/activate
```
Windows:
```bash
$ venv/Scripts/activate
```

Install needed requirements:
```bash
(venv) $ pip install -r requirements.txt
``` -->

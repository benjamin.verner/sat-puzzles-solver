class Solver:
    def __init__(self, SAT):
        self.solution = [None] * len(SAT.variables)
        self.clause_checker = [[] for i in range(2 * len(SAT.variables))]
        for clause in SAT.clauses:
            self.clause_checker[clause[0]].append(clause)



    def check_clauses(self, variable_index, truth_value):
        """
        checking whether all clauses assigned to the given variable are either
        fulfilled by its value or if they can be reassigned to a different variable
        """

        # reverse to the variable value, clauses with this value have to be reassigned
        not_variable_value = variable_index * 2 + truth_value

        change_log = []
        # for clause in self.clause_checker[not_variable_value]:
        while self.clause_checker[not_variable_value]:#####
            clause = self.clause_checker[not_variable_value][0]#####
            reassigned = False

            for literal in clause:
                variable = literal // 2
                is_positive = 1 - literal % 2
                if self.solution[variable] is None or self.solution[variable] == is_positive:
                    # if variable is not assigned or has the same value as var, thus the clause can be reassigned to it
                    # change_log.append([value, clause])
                    del self.clause_checker[not_variable_value][0]#####
                    self.clause_checker[literal].append(clause)#####
                    reassigned = True
                    break

            if not reassigned:
                return False

        # self.clause_checker[not_variable_value] = []
        # for val, cl in change_log:
        #     self.clause_checker[val].append(cl)
        return True



    def find_solutions(self, puzzle, print_incomplete = False, depth = 0):
        if depth == len(puzzle.variables):
            yield self.solution
            return

        elif print_incomplete:
            puzzle.print_solution(self.solution, completed = False)

        for truth_value in [0, 1]:
            self.solution[depth] = truth_value
            if self.check_clauses(depth, truth_value):
                for solution in self.find_solutions(puzzle, print_incomplete, depth + 1):
                    yield solution

        self.solution[depth] = None
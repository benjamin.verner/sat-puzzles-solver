from logik import Logik
from easy_as_abc import ABC
from sudoku import Sudoku
from slitherlink import Slitherlink

class SAT:
    def __init__(self):
        self.variables = []
        self.variable_values = dict()
        self.clauses = []
        self.type = None
        self.abc_zadani = None



    def parse(self, assignment, type):
        '''
        Creates an object of the problem type, then calls its function for creating clauses
        '''

        self.type = type
        if self.type == 'sudoku':
            self.type = Sudoku()
            for information in self.type.generate_clauses(assignment):
                self.convert_clauses(information)
        elif self.type == 'abc':
            self.type = ABC()
            it = self.type.generate_clauses(assignment)
            self.abc_zadani = next(it) # first line of the ABC input has to be a set of letters used
            for information in it:
                self.convert_clauses(information)
        elif self.type == 'logik':
            self.type = Logik()
            for information in self.type.generate_clauses(assignment):
                self.convert_clauses(information)
        elif self.type == 'slitherlink':
            self.type = Slitherlink()
            for information in self.type.generate_clauses(assignment):
                self.convert_clauses(information)

        elif self.type == 'test':
            vstup = open('vstup.txt')
            for information in vstup:
                if len(information) > 0:
                    self.convert_clauses(information)



    def convert_clauses(self, information):
        '''
         Sets track of all new variables used in the clause, convert all variables to integers, return as a tuple
        (each variable is given an index, its True value is index * 2, its False value index * 2 + 1)
        '''

        clause = set()
        for variable_value in information.split():
            negated = 1 if variable_value[0] == '~' else 0
            variable_value = variable_value[negated:]
            if variable_value not in self.variable_values:
                self.variable_values[variable_value] = len(self.variables)
                self.variables.append(variable_value)
            clause_literal = self.variable_values[variable_value] << 1 | negated
            clause.add(clause_literal)
        self.clauses.append(tuple(clause))



    def print_solution(self, solution, completed = True):
        if type(self.type) == str:
            print(solution)
        else:
            self.type.print_solution(solution, self, completed)
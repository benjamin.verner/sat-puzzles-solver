pip>=21.3.1
keyring>=18.0.1
setuptools>=45.2.0
Cython>=0.29.14
plumbum>=1.7.1
rich>=10.14.0